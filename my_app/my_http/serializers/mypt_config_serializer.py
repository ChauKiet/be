from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ..models.mypt_config import MyPTConfig

class MyPTConfigSerializer(ModelSerializer):
    class Meta:
        model = MyPTConfig
        fields = '__all__'