from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from ..models.mypt_config_group import MyPTConfigGroup


class MyPTConfigGroupSerializer(ModelSerializer):
    class Meta:
        model = MyPTConfigGroup
        fields = ['group_id', 'group_title', 'group_status']
        # fields = '__all__'


from ..models.mypt_config import MyPTConfig
class MyPTConfigSerializer(ModelSerializer):
    class Meta:
        model = MyPTConfig
        fields = '__all__'