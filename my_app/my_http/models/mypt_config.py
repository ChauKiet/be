from django.db import models


class MyPTConfig(models.Model):
    class Meta:
        db_table = 'mypt_managers_configs'

    TYPE_VALUES = [
        ('constant', 'constant'),
        ('message', 'message'),
        ('remote', 'remote'),
    ]

    STATUS_VALUES = [
        ('enabled', 'enabled'),
        ('disabled', 'disabled'),
        ('deleted', 'deleted'),
    ]

    config_id = models.IntegerField(primary_key=True)
    config_type = models.CharField(max_length=10, choices=TYPE_VALUES)
    config_key = models.CharField(max_length=100, unique=True)
    group_id = models.IntegerField()
    config_value = models.TextField(blank=True, null=True)
    config_description_vi = models.TextField(blank=True, null=True)
    config_description_en = models.TextField(blank=True, null=True)
    config_status = models.CharField(max_length=10, choices=STATUS_VALUES, default='enabled')
    owner = models.CharField(max_length=100)
    date_created = models.DateTimeField(auto_now_add=True)
    date_last_updated = models.DateTimeField(auto_now=True)
    note = models.CharField(max_length=255)

    def __str__(self):
        return self.config_key