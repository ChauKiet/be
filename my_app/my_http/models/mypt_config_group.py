from django.db import models


class MyPTConfigGroup(models.Model):
    class Meta:
        db_table = 'mypt_managers_group_configs'

    STATUS_VALUES = [
        ('enabled', 'enabled'),
        ('disabled', 'disabled'),
        ('deleted', 'deleted'),
    ]

    group_id = models.IntegerField(primary_key=True)
    group_title = models.CharField(max_length=100)
    group_key = models.TextField(null=True, blank=True)
    group_description = models.TextField(blank=True, null=True)
    group_status = models.CharField(max_length=10, choices=STATUS_VALUES, default='enabled')
    email_created = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    note = models.CharField(max_length=255)

    def __str__(self):
        return self.group_title