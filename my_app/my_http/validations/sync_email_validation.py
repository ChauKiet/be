from rest_framework.serializers import *

from my_app.my_http.models.user_infos import UserInfos
from my_app.my_http.models.emails_prepare_add_pers_fea_roles import EmailsPrepareAddPersFeaRoles
from my_app.my_http.models.mypt_user_permission import MyPTUserPermission
from my_app.my_http.models.ho_user_permission import HoUserPermission

class SyncEmailValidation(Serializer):
    old_email = EmailField(required=True)
    new_email = EmailField(required=True)

    def create(self, validated_data):
        old_email = validated_data['old_email']
        new_email = validated_data['new_email']

        EmailsPrepareAddPersFeaRoles.objects.filter(email=old_email).update(email=new_email)

        if not UserInfos.objects.filter(email=new_email).exists():
            if UserInfos.objects.filter(email=old_email).exists():
                UserInfos.objects.filter(email=old_email).update(email=new_email)
            else:
                UserInfos.objects.create(email=new_email)
        else:
            old_user_id = UserInfos.objects.get(email=old_email).user_id

            mypt_permissions = MyPTUserPermission.objects.filter(user_id=old_user_id)
            for permission in mypt_permissions:
                MyPTUserPermission.objects.create(
                    user_id=UserInfos.objects.get(email=new_email).user_id,
                    permission_id=permission.permission_id,
                    permission_code=permission.permission_code,
                    child_depart=permission.child_depart,
                    date_created=permission.date_created,
                    date_modified=permission.date_modified,
                    updated_by=permission.updated_by,
                    created_by=permission.created_by,
                )

            ho_permissions = HoUserPermission.objects.filter(user_id=old_user_id)
            for permission in ho_permissions:
                HoUserPermission.objects.create(
                    user_id=UserInfos.objects.get(email=new_email).user_id,
                    permission_id=permission.permission_id,
                    permission_code=permission.permission_code,
                    child_depart=permission.child_depart,
                    date_created=permission.date_created,
                    date_modified=permission.date_modified,
                    updated_by=permission.updated_by,
                    created_by=permission.created_by,
                    assign_method=permission.assign_method,
                )

        return validated_data
