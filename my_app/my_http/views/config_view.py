# Models
from ..models.mypt_config_group import *
from ..models.mypt_config import *
# Serializers
from ..serializers.mypt_config_group_serializer import *
from ..serializers.mypt_config_serializer import *
# Helpers
from rest_framework.viewsets import ViewSet
from ...my_core.helpers.response import *
from ..paginations.custom_pagination import StandardPagination


class ManagerConfig(ViewSet):
    def configGroup(self, request):
        queryset = MyPTConfigGroup.objects.all()
        serializer = MyPTConfigGroupSerializer(queryset, many=True)
        return response_data(serializer.data)

    def configShow(self, request):
        try:
            group_id = request.data.get('group_id')
            group = MyPTConfigGroup.objects.get(group_id=group_id)
            if group.group_status != 'enabled':
                return response_data({}, statusCode=0, message="Group is not enabled")
            queryset = MyPTConfig.objects.filter(group_id=group_id)
            paginator = StandardPagination()
            paginated_queryset = paginator.paginate_queryset(queryset, request)
            serializer = MyPTConfigSerializer(paginated_queryset, many=True)
            return paginator.get_paginated_response(serializer.data)
        except MyPTConfigGroup.DoesNotExist:
            return response_data({}, statusCode=0, message="group_id does not exist")