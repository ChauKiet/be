from my_app.my_http.serializers.ho_permission_serializer import *
from my_app.my_http.serializers.ho_permission_group_serializer import *
from my_app.my_http.serializers.ho_user_permission_serializer import *


from my_app.my_http.models.ho_permission import *
from my_app.my_http.models.ho_permission_group import *
from my_app.my_http.models.ho_user_permission import *

from my_app.my_http.Entities.log_auto_permisison_handler import *

from django.db import connection

from my_app.my_core.helpers.utils import is_null_or_empty, get_str_datetime_now_import_db
import redis
import ast
from django.conf import settings as project_settings

class PermissionHandler:
    def __init__(self):
        pass

    # API nay se lay ra cac permission cua userId nay, va tap hop cac child_depart de gui qua mypt-ho-profile de lay parent_depart & branch
    def getAllPermissionsByUser(self, userId):
        query = "SELECT user_per.permission_code, per_group.permission_group_code, user_per.child_depart, has_depart_right FROM " + HoUserPermission._meta.db_table + " AS user_per INNER JOIN " + HoPermission._meta.db_table + " AS per ON user_per.permission_id = per.permission_id INNER JOIN " + HoPermissionGroup._meta.db_table + " AS per_group ON per.permission_group_id = per_group.permission_group_id WHERE user_per.user_id = " + str(userId) + " AND per.is_deleted = 0 AND per_group.is_deleted = 0"
        cursor = connection.cursor()
        cursor.execute(query)
        userPerRows = cursor.fetchall()
        persData = {}
        collectedChildDeparts = []
        print(userPerRows)

        redisInstance = redis.StrictRedis(host=project_settings.REDIS_HOST_CENTRALIZED
                                          , port=project_settings.REDIS_PORT_CENTRALIZED
                                          , db=project_settings.REDIS_DATABASE_CENTRALIZED,
                                          password=project_settings.REDIS_PASSWORD_CENTRALIZED
                                          , decode_responses=True, charset="utf-8")
        # lay danh sach Branch
        allBranchesStr = redisInstance.get("allBranches")
        allBranchesFromRedis = ast.literal_eval(allBranchesStr)
        print("all branch tu Redis :")
        print(allBranchesFromRedis)

        for userPerRow in userPerRows:
            perCode = userPerRow[0]
            hasDepartRight = int(userPerRow[3])
            persData[perCode] = {
                "group_code": userPerRow[1],
                "has_depart_right": hasDepartRight,
                "child_depart_rights": {},
                "branch_rights": {},
                "all_child_depart_rights": [],
                "specificChildDeparts": []
            }

            # phan tach child_depart
            if hasDepartRight == 1:
                childDepartStr = userPerRow[2].strip()
                if childDepartStr != "":
                    childDepartArrs = childDepartStr.split(",")
                    # Neu co ALL trong childDepartArrs thi ko quan tam cac phan tu khac nua
                    if "ALL" in childDepartArrs:
                        # print("[perCode " + perCode + "] : co ALL trong childDepart Str ! Ko quan tam cac phan tu khac trong childDepart Str nua !")
                        for branchStr in allBranchesFromRedis:
                            persData[perCode]["branch_rights"][branchStr] = ["ALL"]
                    else:
                        # Neu khong co ALL trong childDepartArrs thi se bat dau di qua tung child depart trong childDepartArrs
                        # Di qua tung branch de tao list empty truoc
                        for branch_str in allBranchesFromRedis:
                            persData[perCode]["branch_rights"][branch_str] = []
                        # Di qua tung child depart trong childDepartArrs
                        for childDepartItem in childDepartArrs:
                            hasAllBranch = False
                            for branch_code in allBranchesFromRedis:
                                all_branch_str = "ALL" + branch_code
                                # Neu childDepartItem chinh la 1 branch
                                if childDepartItem == all_branch_str:
                                    # print("[perCode " + perCode + "] : voi child depart " + childDepartItem + " : hasAllBranch la TRUE !")
                                    persData[perCode]["branch_rights"][branch_code] = ["ALL"]
                                    hasAllBranch = True

                            if hasAllBranch == False:
                                # print("[perCode " + perCode + "] : voi child depart " + childDepartItem + " : hasAllBranch la false ne")
                                persData[perCode]["specificChildDeparts"].append(childDepartItem)
                                if childDepartItem not in collectedChildDeparts:
                                    collectedChildDeparts.append(childDepartItem)


                        # if childDepartItem == "ALL":
                        #     persData[perCode]["branch_rights"] = {
                        #         "TIN": ["ALL"],
                        #         "PNC": ["ALL"],
                        #         "FTEL": ["ALL"]
                        #     }
                        # elif childDepartItem == "ALLTIN":
                        #     persData[perCode]["branch_rights"]["TIN"] = ["ALL"]
                        # elif childDepartItem == "ALLPNC":
                        #     persData[perCode]["branch_rights"]["PNC"] = ["ALL"]
                        # elif childDepartItem == "ALLFTEL":
                        #     persData[perCode]["branch_rights"]["FTEL"] = ["ALL"]
                        # else:
                        #     persData[perCode]["specificChildDeparts"].append(childDepartItem)
                        #     if childDepartItem not in collectedChildDeparts:
                        #         collectedChildDeparts.append(childDepartItem)
            else:
                persData[perCode]["child_depart_rights"] = None
                persData[perCode]["branch_rights"] = None


        return {
            "persData": persData,
            "collectedSpecificChildDeparts": collectedChildDeparts
        }

    def getPermissionByUserId(self, userId):
        query = "SELECT user_per.permission_code, per_group.permission_group_code, user_per.child_depart, user_per.agency, user_per.parent_depart, user_per.branch, has_depart_right FROM " + HoUserPermission._meta.db_table + " AS user_per INNER JOIN " + HoPermission._meta.db_table + " AS per ON user_per.permission_id = per.permission_id INNER JOIN " + HoPermissionGroup._meta.db_table + " AS per_group ON per.permission_group_id = per_group.permission_group_id WHERE user_per.user_id = " + str(userId) + " AND per.is_deleted = 0 AND per_group.is_deleted = 0"
        cursor = connection.cursor()
        cursor.execute(query)
        userPerRows = cursor.fetchall()

        persData = {}
        collectedSpecificChildDeparts = []
        collectedSpecificAgencies = []
        collectedSpecificParentDeparts = []

        for row in userPerRows:
            persData[row[0]] = {
                "group_code": row[1],
                "on_child_departs": row[2].split(",") if row[2] else [],
                "on_agencies": row[3].split(",") if row[3] else [],
                "on_parent_departs": row[4].split(",") if row[4] else [],
                "on_branches": row[5].split(",") if row[5] else [],
                "has_depart_right": row[6]
            }

        return {
            "persData": persData,
            "collectedSpecificChildDeparts": collectedSpecificChildDeparts,
            "collectedSpecificAgencies": collectedSpecificAgencies,
            "collectedSpecificParentDeparts": collectedSpecificParentDeparts
        }

    def provide_permission(self, dict_data):

        # kiem tra va cap quyen cho user id , permission_id la kieu int
        list_err = []
        user_id = dict_data['userId']
        permission_id = dict_data['permissionId']
        # right_on_child_depart = dict_data['childDepart']
        queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id)
        if not queryset.exists():
            # them moi value
            serializer = InfoHoUserPermissionSerializer(data=dict_data)
            if serializer.is_valid():
                serializer.save()
            else:
                list_err.append(serializer.errors)
        else:
            # update value
            serializer = InfoHoUserPermissionSerializer(queryset.first(), data=dict_data)
            if serializer.is_valid():
                serializer.save()
            else:
                list_err.append(serializer.errors)
        return list_err

    def provide_permission_v2(self,dict_save):
        # cap quyen theo chi nhanh, hoac phong ban
        list_err = []
        right_on_child_depart = dict_save.get('childDepart', '')
        i_permission = dict_save.get("permissionId")
        has_right_on_child_depart = dict_save.get('has_right_on_child_depart', 1)
        user_id = dict_save.get('userId')

        queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=i_permission)

        try:

            if queryset.exists():
                old_right_on_child_depart = queryset.values_list('child_depart', flat=True)[0]
                if old_right_on_child_depart == right_on_child_depart:
                    return list_err
                if has_right_on_child_depart == 1:
                    # old_right_on_child_depart = queryset.values_list('child_depart', flat=True)[0]

                    # todo truong hop co quyen ALL, ALLTIN, ALLPNC
                    if old_right_on_child_depart == "ALL":
                        return list_err
                    # elif old_right_on_child_depart == "ALLTIN" or old_right_on_child_depart == "ALLPNC":
                    #     serializer = InfoHoUserPermissionSerializer(queryset.first(), data=dict_save)
                    #     if serializer.is_valid():
                    #         serializer.save()
                    #     else:
                    #         list_err.append(str(serializer.errors))
                    else:
                        if not is_null_or_empty(old_right_on_child_depart):
                            # neu co phong ban roi thi bo sung nhung phong ban chua co
                            list_old_child_depart = old_right_on_child_depart.split(",")
                            list_new_child_depart_input = right_on_child_depart.split(",")

                            list_add_child_depart = [x for x in list_new_child_depart_input if x not in list_old_child_depart]
                            if len(list_add_child_depart) == 0:
                                return list_err
                            str_add_child_depart = ",".join(list_add_child_depart)

                            new_right_on_child_depart = old_right_on_child_depart + "," + str_add_child_depart
                            dict_save.update({
                                "childDepart": new_right_on_child_depart
                            })
                else:
                    dict_save.update({
                        "childDepart": ""
                    })

                # update data
                dict_save.update({
                    "dateModified": get_str_datetime_now_import_db()
                })
                serializer = InfoHoUserPermissionSerializer(queryset.first(), data=dict_save)
                if serializer.is_valid():
                    serializer.save()
                else:
                    list_err.append(str(serializer.errors))
            else:
                # insert data
                serializer = InfoHoUserPermissionSerializer(data=dict_save)
                if serializer.is_valid():
                    serializer.save()
                else:
                    list_err.append(str(serializer.errors))
            return list_err
        except Exception as ex:
            list_err.append(str(ex))
            return list_err

    def provide_permission_v3(self,dict_save):


        # cap quyen theo chi nhanh, hoac phong ban theo bang user permission
        list_err = []
        i_permission = dict_save.get("permissionId")
        has_right_on_child_depart = dict_save.get('has_right_on_child_depart', 1)
        user_id = dict_save.get('userId')

        queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=i_permission)

        try:

            if queryset.exists():
                if has_right_on_child_depart == 1:
                    branch = dict_save.get("branch", "")
                    parent_depart = dict_save.get("parentDepart", "")
                    agency = dict_save.get("agency", "")
                    child_depart = dict_save.get("childDepart", "")
                    # user_id = dict_save.get("userID")
                    # permission_id = dict_save.get('permissionId', '')

                    if not is_null_or_empty(branch):

                        new_right_on_branch = self.provide_permision_on_depart(queryset, branch, "branch")
                        if new_right_on_branch != "":
                            dict_save.update({
                                "branch": new_right_on_branch
                            })

                    if not is_null_or_empty(parent_depart):
                        new_right_on_parent_depart = self.provide_permision_on_depart(queryset, parent_depart,
                                                                                      "parent_depart")
                        if new_right_on_parent_depart != "":
                            dict_save.update({
                                "parentDepart": new_right_on_parent_depart
                            })

                    if not is_null_or_empty(agency):
                        new_right_on_agency = self.provide_permision_on_depart(queryset, agency,"agency")
                        if new_right_on_agency != "":

                            dict_save.update({
                                "agency": new_right_on_agency
                            })

                    if not is_null_or_empty(child_depart):
                        new_right_on_child_depart = self.provide_permision_on_depart(queryset, child_depart, "child_depart")

                        if new_right_on_child_depart != "":
                            dict_save.update({
                                "childDepart": new_right_on_child_depart
                            })

                else:
                    dict_save.update({
                        "childDepart": ""
                    })

                # update data
                dict_save.update({
                    "dateModified": get_str_datetime_now_import_db()
                })
                serializer = InfoHoUserPermissionSerializer(queryset.first(), data=dict_save)
                # print(dict_save)
                if serializer.is_valid():
                    serializer.save()
                else:
                    list_err.append(str(serializer.errors))
            else:
                # insert data
                serializer = InfoHoUserPermissionSerializer(data=dict_save)
                if serializer.is_valid():
                    serializer.save()
                else:
                    list_err.append(str(serializer.errors))
            return list_err
        except Exception as ex:
            list_err.append(str(ex))
            return list_err


    def provide_permision_on_depart(self, queryset, depart, type_depart):
        old_right_on_depart = queryset.values_list(type_depart, flat=True)[0]
        list_old_depart = old_right_on_depart.split(",")
        list_new_depart_input = depart.split(",")


        list_add_child_depart = [x for x in list_new_depart_input if
                                 x not in list_old_depart]
        if len(list_add_child_depart) == 0:
            return ""
        str_add_branch = ",".join(list_add_child_depart)
        if not is_null_or_empty(old_right_on_depart):
            new_right_on_branch = old_right_on_depart + "," + str_add_branch

            return new_right_on_branch
        return str_add_branch




    def get_right_on_child_depart_on_permission_id_and_user_id(self, user_id, permission_id):
        right_on_permission = ""
        try:
            queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id).values_list('child_depart', flat=True)
            if len(queryset) > 0:
                right_on_permission = queryset[0]

        except Exception as ex:
            print("get_right_on_child_depart_on_permission_id_and_user_id >> error/loi: {}".format(ex))
        return  right_on_permission



    def get_info_permission_from_list_permission_id(self, list_permission_id):
        dict_data = {}
        try:
            queryset = HoPermission.objects.filter(is_deleted=0, permission_id__in=list_permission_id).values('permission_id', 'permission_code', 'has_depart_right')
            if len(queryset) > 0:

                for i in queryset:
                    permission_id = i['permission_id']
                    permission_code = i['permission_code']
                    has_depart_right = i['has_depart_right']
                    dict_data.update({
                        permission_id:{
                            "permission_code": permission_code,
                            "has_depart_right": has_depart_right
                        }
                    })

        except Exception as ex:
            print("get_info_permission_from_list_permission_id >> Error/loi: {}".format(ex))
        return dict_data

    def delete_permission_on_agency(self, right_on_child_depart, user_id, k_permission_id):
        # k_permission_id = permissionId
        # phai kiem tra type_action = delete, right_on_child_depart la chi nhanh
        list_right_on_child_depart = right_on_child_depart.split(",")
        for i_child_depart in list_right_on_child_depart:
            queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=k_permission_id,
                                                       child_depart__icontains=i_child_depart)

            if queryset.exists():
                old_right_child_depart = queryset.values_list('child_depart', flat=True)[0]
                # print(140)
                # print(k_permission_id)
                # print(old_right_child_depart)
                # print(queryset.values_list('child_depart'))

                list_old_child_depart = old_right_child_depart.split(",")
                list_old_child_depart.remove(i_child_depart)


                # new_child_depart = old_right_child_depart.replace(i_child_depart, "")
                new_child_depart = ",".join(list_old_child_depart)
                queryset.update(child_depart=new_child_depart)

        queryset_new = HoUserPermission.objects.filter(user_id=user_id, permission_id=k_permission_id)
        if len(queryset_new) > 0:
            new_child_depart = queryset_new.values_list('child_depart', flat=True)[0]
            if new_child_depart == "":
                save_log_class_delete = LogAutoPermissionHandler()
                save_log_class_delete.save_log_permission(str(queryset_new.values()), "data_delete",
                                                          "")
                queryset_new.delete()

    def delete_permission_on_user(self, dict_save):

        branch = dict_save.get("branch", "")
        parent_depart = dict_save.get("parentDepart", "")
        agency = dict_save.get("agency", "")
        child_depart = dict_save.get("childDepart", "")
        user_id = dict_save.get("userId")
        permission_id = dict_save.get('permissionId', '')


        # dict_tmp = dict_save.copy()
        if branch == "" and parent_depart == "" and agency == "" and child_depart == "":
            # truong hop quyen khong ap dung tren phong ban
            queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id)
            if queryset.exists():
                queryset.delete()

        if not is_null_or_empty(branch):
            list_branch = branch.split(",")
            for i_branch in list_branch:
                queryset = HoUserPermission.objects.filter(user_id=user_id,permission_id=permission_id, branch__icontains=i_branch)
                if queryset.exists():
                    new_branch = self.delete_permission_on_depart(queryset, "branch", i_branch)
                    queryset.update(branch=new_branch)

            # # lay neu la khoang trang thi bo di dong do lun
            # self.delete_depart_space_on_user_id_and_permission_id(user_id, permission_id)

        if not is_null_or_empty(parent_depart):
            list_parent_depart = parent_depart.split(",")
            for i_parent_depart in list_parent_depart:
                queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id,
                                                           parent_depart__icontains=i_parent_depart)
                if queryset.exists():
                    new_parent_depart = self.delete_permission_on_depart(queryset, "parent_depart", i_parent_depart)
                    queryset.update(parent_depart=new_parent_depart)

        if not is_null_or_empty(agency):
            list_agency = agency.split(",")
            for i_agency in list_agency:
                queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id,
                                                           agency__icontains=i_agency)
                print(queryset.exists())
                if queryset.exists():
                    new_agency = self.delete_permission_on_depart(queryset, "agency", i_agency)
                    queryset.update(agency=new_agency)


        if not is_null_or_empty(child_depart):
            list_child_depart = child_depart.split(",")
            for i_child_depart in list_child_depart:
                queryset = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id,
                                                           child_depart__icontains=i_child_depart)
                if queryset.exists():
                    new_child_depart = self.delete_permission_on_depart(queryset, "child_depart", i_child_depart)
                    queryset.update(child_depart=new_child_depart)

        # delete dong co branch va child_depart rong
        queryset_new = HoUserPermission.objects.filter(user_id=user_id, permission_id=permission_id)
        if queryset_new.exists():
            info_depart_of_user_id = queryset_new.values('child_depart', 'branch', 'agency', 'parent_depart')[0]
            child_depart_new = info_depart_of_user_id['child_depart']
            branch_new = info_depart_of_user_id['branch']
            agency_new = info_depart_of_user_id['agency']
            parent_depart_new = info_depart_of_user_id['parent_depart']
            if child_depart_new == "" and branch_new == "" and agency_new == "" and parent_depart_new == "":
                queryset_new.delete()

    def delete_permission_on_depart(self, queryset, type_depart, depart):
        old_depart = queryset.values_list(type_depart, flat=True)[0]
        list_old_depart = old_depart.split(",")
        list_old_depart.remove(depart)
        new_depart = ",".join(list_old_depart)

        return new_depart
















    def get_list_permission_id_from_user_id(self, user_id):
        list_permission_id = []
        try:
            queryset = HoUserPermission.objects.filter(user_id=user_id).values_list('permission_id', flat=True)
            list_permission_id = queryset
        except Exception as ex:
            print("get_list_permission_id_from_user_id >> {}".format(ex))
        return list_permission_id

