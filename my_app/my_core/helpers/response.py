from rest_framework.response import Response
from rest_framework import status

def response_data(data, statusCode=1, message="Success"):
    result = {
        'statusCode': statusCode,
        'message': message,
        'data': data
    }
    return Response(result, status.HTTP_200_OK)

def response_paginator(total, per_page, data,count=0):
    result = {
        'max_page': ceil(total/per_page),
        'list_data': data,
        'count':count
    }
    return response_data(data=result)